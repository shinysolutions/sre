### What is this repository for? ###

* This component will import Sailwave HTML Files into a  Joomla Article.
* 0.31 - This is a BETA version only for use on production servers after careful testing.


### How do I get set up? ###

* Download the [package](https://bitbucket.org/shinysolutions/sre/downloads/pkg_sailwaveimporter.zip) and install it in Joomla via the Administration System.
* Visit the sailwave page it creates in Joomla and complete the Configuration Checks
* Make sure to enable the plugins
* Download the [windows installer](https://bitbucket.org/shinysolutions/sre/downloads/SailwaveImporter-WindowsInstaller.exe) to update Sailwave: Add the sailwave template files and refresh sailwaves publishing menu
* Publish from Sailwave to Joomla using FTP
* Content is auto created based on your settings in the plugin.

### Contribution guidelines ###

* Want to improve the code - feel free please clone the repo make changes and send us a pull request back.

### Who do I talk to? ###

* Send us bug reports using the issue tracker on bitbucket

## CHANGE LOG

### 0.31 ###

Fix for issue #22 where a result with no Series Title in the body would hang the importer for this file and all subsequent files.

### 0.30 ###

Add language file. Creates crude fix for #9 by effectively hiding the fieldsets. Core issue is a fault in Joomla handling of models in configs.

Added plugin status check in config check

Removed broken plugins button, Access via Config Check please

Place close button on top of screen to make returning to SI control panel easier.

Fixes to email notification of failure.

Pretty up the system check catch faults on FTP priv's not allowing file read.

Add ability to make article featured by default or specific article by adding * to event name

### 0.24 ###
Version increment to 0.24 (test XML update)

Added XML update files and incremented version numbers.  Automatic updating will now be offered.

Added SailwaveResults as default folder to avoid issue (#13) where root directory used as import folder if no import folder set.

Updated menu links to work in J!3.5 (#7)