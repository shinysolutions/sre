<?php

/*
 *  version: 0.31
 *  author: Shiny Solutions
 *  copyright Copyright (C) 2016 Shiny Solutions. All Rights Reserved.
 *  @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die("Restricted access");


class SailwaveImporterModelImported extends JModelItem
{
	/**
	 * @var object item
	 */
	protected $item;
 
           
 
	/**
	 * Get the message
	 * @return object The message to be displayed to the user
	 */
	public function getItems()
	{
                $application = JFactory::getApplication();
                $importer_params = JComponentHelper::getParams('com_sailwaveimporter');
                $sailwaveFolder = JPATH_SITE . "/" . $importer_params->get('sailwave_folder', '');
                $quarantineFolder = $sailwaveFolder . "/" . "Quarantine";
                $processingFolder = $sailwaveFolder . "/" . "Processing";
                $importedFolder = $sailwaveFolder . "/" . "Imported";
                $ignoredFiles = array_map('strtolower' , array ('.', '..', '.htaccess'));


		if (!isset($this->items)) 
		{


                    //Scan the importeds folder for any new files to import for processing
                    $imported = array_diff(scandir($importedFolder), $ignoredFiles);
                    //Exclude directories
                    $imported = array_filter( $imported, function ($item) use($importedFolder){
                        return !is_dir($importedFolder. "/" . $item );
                    });
                    $this->items = $imported ;
		}
		return $this->items;
	}
}