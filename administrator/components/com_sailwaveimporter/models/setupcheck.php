<?php

/*
 *  version: 0.31
 *  author: Shiny Solutions
 *  copyright Copyright (C) 2016 Shiny Solutions. All Rights Reserved.
 *  @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die("Restricted access");


class SailwaveImporterModelSetupcheck extends JModelItem
{
	/**
	 * @var object item
	 */
	protected $item;
 
           
 
	/**
	 * Get the message
	 * @return object The message to be displayed to the user
	 */
	public function getItems()
	{
                $application = JFactory::getApplication();
                $importer_params = JComponentHelper::getParams('com_sailwaveimporter');

                
		if (!isset($this->items)) 
		{
                    $db = JFactory::getDbo();
 
                    $query = $db->getQuery(true);

                    $query
                        ->select($db->quoteName(array('extension_id', 'name', 'element', 'enabled', 'manifest_cache')))
                        ->from($db->quoteName('#__extensions'))
                        ->where($db->quoteName('element') . ' LIKE '. $db->quote('%sailwave%'). ' AND ' . $db->quoteName('type') . ' = ' . $db->quote('plugin'));

                    
                    // Reset the query using our newly populated query object.
                    $db->setQuery($query);

                    // Load the results as a list of stdClass objects (see later for more options on retrieving data).
                    $this->items = $db->loadObjectList();

		}
		return $this->items;
	}
}