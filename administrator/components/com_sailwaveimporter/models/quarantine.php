<?php

/*
 *  version: 0.31
 *  author: Shiny Solutions
 *  copyright Copyright (C) 2016 Shiny Solutions. All Rights Reserved.
 *  @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die("Restricted access");


class SailwaveImporterModelQuarantine extends JModelItem
{
	/**
	 * @var object item
	 */
	protected $item;
 
           
 
	/**
	 * Get the message
	 * @return object The message to be displayed to the user
	 */
	public function getItems()
	{
                $application = JFactory::getApplication();
                $importer_params = JComponentHelper::getParams('com_sailwaveimporter');
                $sailwaveFolder = JPATH_SITE . "/" . $importer_params->get('sailwave_folder', '');
                $quarantineFolder = $sailwaveFolder . "/" . "Quarantine";
                $processingFolder = $sailwaveFolder . "/" . "Processing";
                $importedFolder = $sailwaveFolder . "/" . "Imported";
                $ignoredFiles = array_map('strtolower' , array ('.', '..', '.htaccess'));


		if (!isset($this->items)) 
		{


                    //Scan the importeds folder for any new files to import for processing
                    $quarantined = array_diff(scandir($quarantineFolder), $ignoredFiles);
                    //Exclude directories
                    $quarantined = array_filter( $quarantined, function ($item) use($quarantineFolder){
                        return !is_dir($quarantineFolder. "/" . $item );
                    });
                    $this->items = $quarantined ;
		}
		return $this->items;
	}
}