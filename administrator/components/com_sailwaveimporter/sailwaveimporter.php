<?php

/* 
# version: 0.31
# author: Shiny Solutions
# copyright Copyright (C) 2016 Shiny Solutions. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/

defined('_JEXEC') or die("Restricted access");

define('SI_admin_access', TRUE);

$document = JFactory::getDocument();
$document->addStyleSheet('/media/com_sailwaveimporter/CSS/style.css');


$controller = JControllerLegacy::getInstance('SailwaveImporter');
 
// Perform the Request task
$input = JFactory::getApplication()->input;
$controller->execute($input->getCmd('task'));
 
// Redirect if set by the controller
$controller->redirect();

?>
