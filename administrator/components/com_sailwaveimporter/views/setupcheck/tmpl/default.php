<?php

/*
 *  version: 0.31
 *  author: Shiny Solutions
 *  copyright Copyright (C) 2016 Shiny Solutions. All Rights Reserved.
 *  @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die("Restricted access");
include(JPATH_BASE.'/components/com_sailwaveimporter/helpers/systemcheck.php');

?>


<h2>Sailwave Results Folder</h2>
<P>Sailwave Results Folder is defined as: <?php echo $sailwaveFolder ?></p>
<p>This folder <?php echo $sailwaveFolderMSG ?></p>
<h2>Quarantine Folder</h2>
<p><?php echo $quarantineFolderMSG ?></p>
<h2>Processing Folder</h2>
<p><?php echo $processingFolderMSG ?></p>
<h2>Imported Folder</h2>
<p><?php echo $importedFolderMSG ?></p>
<h2>Plugins</h2>

<?php 
if (count( $this->items ) <1 ) { 
echo "SW_ISSUE" . "No plugins were found - this will significantly affect the functionality of the component!"   ; 
}  else {
    

?>


<table>
    <thead><th>Status</th><th>Plugin</th><th>Comment</th></thead>
    <?php 
    foreach ($this->items as $row ) {
        if ($row->enabled != 1 ) {
            $status = "<span class='icon-unpublish'></span>";
        } else {
            $status = "<span class='icon-publish'></span>";
        }
        $manifest = json_decode($row->manifest_cache, true);
        $description = $manifest[description];       
        echo "
        <tr>
            <td>" .$status    ." </td>
            <td><a href='index.php?option=com_plugins&view=plugin&task=plugin.edit&extension_id=".$row->extension_id."'>" .$row->name ."</a> </td>
            <td>".$description."</td>
        </tr>";
        
 
    }
    ?>
</table>
<?php }

?>