<?php

/*
 *  version: 0.31
 *  author: Shiny Solutions
 *  copyright Copyright (C) 2016 Shiny Solutions. All Rights Reserved.
 *  @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die("Restricted access");

?>
<h1>Sailwave Results Files Imported into Joomla</h1> 
<P>Listed below are all the Sailwave Results Files that have previously been imported into Joomla.  Deleting these files will not delete the imported result.  If you wish to delete the imported result you need to trash the result as an article in the Joomla Article Manager</p>

<table>
    <thead><th>Filename</th></thead>
<?php 
foreach ($this->items as $item) { 
   echo "<tr><td>";
   echo  $item;
   echo " </td></tr>";
    
}?>
</table>