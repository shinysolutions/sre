<?php

/*
 *  version: 0.31
 *  author: Shiny Solutions
 *  copyright Copyright (C) 2016 Shiny Solutions. All Rights Reserved.
 *  @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die("Restricted access");
//$info = parse_url($url);
//$host = $info['host'];
$host = $_SERVER['HTTP_HOST'];
$host_names = explode(".", $host);
//$bottom_host_name = $host_names[count($host_names)-2] . "." . $host_names[count($host_names)-1];
$host_names = array_slice($host_names,1,count($host_names));
$bottom_host_name = implode(".", $host_names);
?>
<h1>Help for Race Officers</h1> 
<P> These pages aim help Race Officers and Scorers to use Sailwave to correctly upload their results to a website running Sailwave Importer</p>
<P> This help guide is split into two sections - <a href="#setup">Initial Sailwave Setup</a> and <a href="#publishing">Routine Results Publishing</a>.  Obviously the Initial Setup should only need to be done once per Sailwave Installation</p>
<h2 id="setup">Sailwave Setup</h2>
<p>These steps should be performed once for a sailwave installation. It may also be necessary to repeat them after a sailwave upgrade.</p>
<p>Prior to starting these steps it is assumed that you already have a current version of sailwave installed on your computer.  If you don't you can download it free from: <a href='http://www.sailwave.com/category/sailwave'>www.sailwave.com</a> and the install it.</P>
<P>Now download the <a href="https://bitbucket.org/shinysolutions/sre/downloads/SailwaveImporter-WindowsInstaller.exe">SailwaveImporter Windows Installer</a> file from https://bitbucket.org/shinysolutions/sre/downloads.  This is a small windows executable file which will install some new templates into Joomla.</p>
<P>Double click on the installer file.  The installer will open.  You should be able to use all the default options</P>


<div style="overflow: auto">
    <div style="float:left; padding-right: 20px; padding-bottom: 20px"> 
        <img src='/media/com_sailwaveimporter/images/help-screens/windowsInstaller1.png' />
    </div>
 
<P>On the first screen click next to confirm you wish to install the importer.</P>
</div>

<div style="overflow: auto">
    <div style="float:left; padding-right: 20px; padding-bottom: 20px"> 
        <img src='/media/com_sailwaveimporter/images/help-screens/windowsInstaller2.png' />
    </div>
 
<P>On the second screen unless you are aware that your sailwave install has been made to a different folder click Next. If sailwave is installed somewhere different you should select the Template folder within your Sailwave Install. </P>
</div>


<div style="overflow: auto">
    <div style="float:left; padding-right: 20px; padding-bottom: 20px"> 
        <img src='/media/com_sailwaveimporter/images/help-screens/windowsInstaller3.png' />
    </div>
 
<P>You should then receive confirmation that the install has completed</P>
</div>

<P><br /><b>There are still a few manual steps you will need to perform</b></P>


<div style="overflow: auto">
    <div style="float:left; padding-right: 20px; padding-bottom: 20px"> 
        <img src='/media/com_sailwaveimporter/images/help-screens/sailwaveConfig1.png' />
    </div>
 

<P>Now open sailwave and create a new result file.</P>
<ul>
    <li>Click the New Series Button on the Toolbar</li>
    <li>Accept the default settings on 1 race with 1 competitor</li>
    <li>Click OK to create the series</li>
</ul>
</div>

<div style="overflow: auto">
    <div style="float:left; padding-right: 20px; padding-bottom: 20px"> 
        <img src='/media/com_sailwaveimporter/images/help-screens/sailwaveConfig2.png' />
    </div>
 

<p>Then score the series.  (You are only doing this to enable the "View" menu which is disabled by default until scoring completed)</p>
<ul>
    <li>Click the Score Series Button on the Tool Bar</li>
    <li>Accept the default settings and Click Score Series on the window that opens</li>
</ul>
</div>


<div style="overflow: auto">
    <div style="float:left; padding-right: 20px; padding-bottom: 20px"> 
        <img src='/media/com_sailwaveimporter/images/help-screens/sailwaveConfig3.png' />
    </div>
 

    <p>Now go to <b>View/Refresh Publish Menu</b>.  This will add a new menu option called "Publish to Joomla" on the Publishing Menu.</p>
</div>

<div style="overflow: auto">
    <div style="float:left; padding-right: 20px; padding-bottom: 20px"> 
        <img src='/media/com_sailwaveimporter/images/help-screens/sailwaveConfig4.png' />
    </div>
 

    <p>Now select <b>Setup/Global Options</b>. A new popup box will appear. Select the FTP Tab</p>



<P>Enter the FTP account information you have been given by your sailing club website administrator.  <b>Note:</b>This is not your Joomla Website Password.</P>

<table>
    <tr>
        <th>Server:</th>
        <td>Your administrator should tell you this but it is likely to be something like: <b><?php echo "ftp.".$bottom_host_name;?></b></td>
    </tr>
       <tr>
        <th>Base URL:</th>
        <td><?php echo JURI::root().""; ?></td>
    </tr>
       <tr>
        <th>User:</th>
        <td>Your administrator will need to tell you this.  It may be something like: <b><?php echo "results@".$bottom_host_name;?></b></td>
    </tr>
       <tr>
        <th>Password:</th>
        <td>Supplied by administrator</td>
    </tr>
    
       <tr>
        <th>Port:</th>
        <td>Unless told otherwise leave this as 21</td>
       </tr>
       
       <tr>
        <th>Use passive transfers:</th>
        <td>Unless told otherwise do not tick this</td>
    </tr>
    
</table>
</div>

Your set-up should now be complete.

<h2 id="publishing">Routine Results Publishing</h2>
<P>Publishing results to the Joomla Importer is really not very different to publishing results elsewhere in Sailwave.  Create your series and enter results as normal.  if you need help with this there is good guidance available at <a href="http://sailwave.com">The Sailwave Website</a>. When you are ready to publish your results go to <b>Publish/Results to Joomla...</b></p>
<div style="overflow: auto">
    <div style="float:left; padding-right: 20px; padding-bottom: 20px"> 
        <img src='/media/com_sailwaveimporter/images/help-screens/publish2-top.png' />
    </div>
    <P>On the dark grey banner at the top of the publishing banner you should see your Sailing Club Name and the Event / Series Name.
        This name will be incorporated into the paragraph of text describing the results on the website.</P>
    <p>Double check you are viewing finishes</P>
</div>

<div style="overflow: auto">
    <div style="float:left; padding-right: 20px; padding-bottom: 20px"> 
        <img src='/media/com_sailwaveimporter/images/help-screens/publish2-top2.png' />
    </div>
    <P>Use the Final or Provisional Button to set the result type.  Do not change the title or replace the @ and # symbols.  Sailwave will change this to say results are provisional as of "current time" on "current date". This will be added to the introductory text.</P>
</div>

<div style="overflow: auto">
    <div style="float:left; padding-right: 20px; padding-bottom: 20px"> 
        <img src='/media/com_sailwaveimporter/images/help-screens/publish2-what.png' />
    </div>
    <P>Select the tables to be published. A general default is to publish all sailed races, for both a series summary and individual race table.  Your event may have specific needs and you can select these just as you would in any Sailwave Publishing Screen.</P>
</div>

<div style="overflow: auto">
    <div style="float:left; padding-right: 20px; padding-bottom: 20px"> 
        <img src='/media/com_sailwaveimporter/images/help-screens/publish2-what2.png' />
    </div>
    <P>If you select notes and have provided some notes on the series notes tab these will be published. Depending how your administrator has configured the importer the notes will move into the introductory text - this is a good way to draw your reader in by commenting on weather, race events etc.  Using HTML markup in the notes box is likely to result in the file being rejected by the importer. </P>
    <P>You can publish a contents list if you wish, and probably should publish the codes used</p> 
</div>
<p><br/>Once you are happy with the items you are publishing - click <b>Next</b></p>

<div style="overflow: auto">
    <div style="float:left; padding-right: 20px; padding-bottom: 20px"> 
        <img src='/media/com_sailwaveimporter/images/help-screens/publish2-ftp.png' />
    </div>
    <P>On the new window that appears select: <b>Destination: A website using FTP</b></P>
    <P>Give your file a suitable filename e.g. SpringSeriesSundayResulst2016.htm <b>The file name must end in .htm</b> or it will be rejected. Depending on your FTP server set-up you may need to specify a folder in front of the filename e.g. SailwaveResults/SundayResults.htm - your webmaster should be able to advise you about this</p> 
</div>
<p>Your result may not appear instantly on your website.  It will be held in a  queue and depending on the set-up of your website this may take a few minutes to process.</p>

<p>When you have updated results the sailwave importer will recognise these automatically as being from the same sailwave file and will update the existing results article. <i>Note:</i> If you would normally publish different files from the one sailwave file (e.g. for different classes) this will not work - they can be uploaded as a single file or you will need to split the sailwave file into two separate events.</p>

