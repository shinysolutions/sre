<?php

/*
 *  version: 0.31
 *  author: Shiny Solutions
 *  copyright Copyright (C) 2016 Shiny Solutions. All Rights Reserved.
 *  @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die("Restricted access");

?>
<h1>Scan Sailwave Results Folder for New Files for Import</h1>
<?php
require_once JPATH_ADMINISTRATOR.'/components/com_sailwaveimporter/helpers/importer.php';
?>