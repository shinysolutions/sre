<?php

/*
 *  version: 0.31
 *  author: Shiny Solutions
 *  copyright Copyright (C) 2016 Shiny Solutions. All Rights Reserved.
 *  @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die("Restricted access");

?>
<h1>Help for Joomla Website Administrators</h1> 
<P> These pages aim help Joomla Website Administrators to Configure their Joomla Site Correctly and provide advice to Race Officers to configure Sailwave Correctly</p>
<p><b>These are the steps you need to undertake.  Its important to try and do them in this order!</B></p>
<ol>
    <li>Create a folder on your server to take your Sailwave Result Files by FTP.  By default this is SailwaveResults.  If you aren't sure how to do this you could set it in SailwaveImport Configuration and then run SailwaveImporter Setup Check and it will create it for you.</li>
    <li>Create an FTP account and give it access to that folder.  <b>For security</b> if possible do not give that FTP account access anywhere else on your site.  If you don't know how to do this speak to your internet server host.</li>
    <li>Go to <a href='index.php?option=com_categories&view=category&layout=edit&extension=com_content'><B>Administrator / Content / Categories / Add New Category</b></a> and create a category for results to be uploaded to.  For example Results or Results-2016</li>
    <li>Open SailwaveImporter's Configuration Screen and set:</li>
    <ul>
        <li>Sailwave Results Category (created above)</li>
        <li>Choose if you want notes to be moved into the intro text (this provides a way for your Race Officers to provide a lure in to the result</li>
        <li>Select if you want an email generated if a result is quarantined and specify who to email</li>
        <li>FTP Folder Created Above</li>
        <li>Permitted extensions if you want anythign other than htm or html to be permitted</li>
        <li>Prohibited contents - consider adding ?PHP to the list</li>
        <li>Required contents - shouldn't need anything added</li>
    </ul>
    <li>Now got to Sailwave Importer's Setup Checker - this should inform you it has created some directories for quarantine files etc. To be sure these were created correctly refresh the page and you should see no error messages - SailwaveImporter will try to add an .htaccess file but on some servers you will need to do this manually. simply add a file to your Results folder called .htaccess which contains just the line deny from all </li>
    <li>Now go to <a href="index.php?option=com_plugins&view=plugins">Extensions / plugins</a> and search for sailwave. 2 plugins should appear:</li>
    <li>Click the Add Sailwave CSS One and type <b>greydays</b> in the CSS file name. Enable the plugin and then save and close it.</li>
    <li>Click the Sailwave Automated Results Importer Plugin.  Change the run frequency if desired (more frequent will update the results sooner but may affect site performance). Enable the plugin and Save and Close it. </li>
    <li>Share the Race Officer Help Page with your Race Officers and send them the FTP logins to enter into sailwave</li>
        
</ol>

