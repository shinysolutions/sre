<?php

/*
 *  version: 0.31
 *  author: Shiny Solutions
 *  copyright Copyright (C) 2016 Shiny Solutions. All Rights Reserved.
 *  @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die("Restricted access");

?>
<H1>Sailwave Importer</H1>

<div class="cpanel">
    <div  style="float:left">
        <div class="icon">
            <a href="index.php?option=com_sailwaveimporter&amp;view=setupcheck" title="Check Set-up">
						<img src="/media/com_sailwaveimporter/icons/testsetup.png" alt="Check Setup">
					
                                                <span>Check Set-up</span></a>
        </div>
    </div>
    
    <div  style="float:left">
        <div class="icon">
            <a href="index.php?option=com_config&view=component&component=com_sailwaveimporter" title="Configuration">
						<img src="/media/com_sailwaveimporter/icons/settings.png" alt="Configuration">
					
                                                <span>Configuration</span></a>
        </div>
    </div>
    

    
    <div  style="float:left">
        <div class="icon">
            <a href="index.php?option=com_sailwaveimporter&amp;view=scan" title="Scan for Imports">
						<img src="/media/com_sailwaveimporter/icons/folder-check.png" alt="Check for Imports">
					
                                                <span>Scan for Results</span></a>
        </div>
    </div>
    
    <div  style="float:left">
        <div class="icon">
            <a href="index.php?option=com_sailwaveimporter&amp;view=quarantine" title="Quarantined Files">
						<img src="/media/com_sailwaveimporter/icons/dangerous.png" alt="Quarantine">
					
                                                <span>View Quarantine</span></a>
        </div>
    </div>
    
    <div  style="float:left">
        <div class="icon">
            <a href="index.php?option=com_sailwaveimporter&amp;view=imported" title="Imported Files">
						<img src="/media/com_sailwaveimporter/icons/imported.png" alt="Imported">
					
                                                <span>View Imported</span></a>
        </div>
    </div>
    
    <div  style="float:left">
        <div class="icon">
            <a href="index.php?option=com_sailwaveimporter&amp;view=webmasterhelp" title="Help for Webmasters">
						<img src="/media/com_sailwaveimporter/icons/joomla.png" alt="help">
					
                                                <span>Help for webmaster</span></a>
        </div>
    </div>
    
      <div  style="float:left">
        <div class="icon">
            <a href="index.php?option=com_sailwaveimporter&amp;view=raceofficerhelp" title="Help for Race Officers">
						<img src="/media/com_sailwaveimporter/icons/lifering.png" alt="help">
					
                                                <span>Help for race officers</span></a>
        </div>
    </div>
    

</div>