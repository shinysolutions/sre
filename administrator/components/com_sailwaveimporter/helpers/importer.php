<?php

/* 
# version: 0.31
# author: Shiny Solutions
# copyright Copyright (C) 2016 Shiny Solutions. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/


defined('_JEXEC') or die("Restricted access");
require_once ('configload.php');

function emailAdmin($emailNotifyBody, $emailQuarantine){
            // Send Email
        $mailer = JFactory::getMailer();
        $config = JFactory::getConfig();
        $sender = array( 
            $config->get( 'mailfrom' ),
            $config->get( 'fromname' ) 
        );
        $mailer->setSender($sender);
        $mailer->addRecipient(array($emailQuarantine));
        $mailer->setSubject('[ERROR] [SailwaveImporter] Unable to Safely Import Files');
        $mailer->setBody($emailNotifyBody);
        $send = $mailer->Send();
}

//Scan the results folder for any new files to import for processing
$import = array_diff(scandir($sailwaveFolder), $ignoredFiles);
//Exclude directories
$import = array_filter( $import, function ($item) use($sailwaveFolder){
        return !is_dir($sailwaveFolder. "/" . $item );
    });


//Check file types
$quarantine = array_filter( $import, function ($item) use($permittedExtensions, $sailwaveFolder){
        return !in_array( strtolower(pathinfo($sailwaveFolder. "/" . $item , PATHINFO_EXTENSION)) , $permittedExtensions);
    });

if (!empty($quarantine)) {
    // Move the file(s) to Quarantine
    foreach($quarantine as $filename) {
        $move = rename($sailwaveFolder . "/" .$filename, $quarantineFolder ."/". $filename );
        if (defined('SI_admin_access')) {
        // Generate Warning on Screen
        $application->enqueueMessage("SailwaveImporter detected a file that could not be imported. For your protection <b>".$filename."</b> has been moved the the quarantine folder.<br />Please review the quarantine view and manually import the file if appropriate.", 'error');
        }
       
        if ($notifyQuarantine  == 1) {
        // Email warning to administrator
            $emailNotifyBody .= $filename ;
            if ($move) {
                $emailNotifyBody .= " - moved to quarantine. \n";
            } else {
                $emailNotifyBody .= " - unable to quarantine. \n";
            //    $emailFooter = "You will receive no further emails because there is a file that could not be quarantined.";                        
            }
        }
    }
    if ($notifyQuarantine ==1) {
            emailAdmin($emailNotifyBody, $emailQuarantine);
    }
}

$import = array_filter( $import, function ($item) use($quarantine) {
    return !in_array($item, $quarantine);
});

if (empty ($import)) {
        if (defined('SI_admin_access')) {
        $application->enqueueMessage("SailwaveImporter did not detect any file that could be imported.", 'information');  
        }
} else {
        if (defined('SI_admin_access')) {
        $application->enqueueMessage("SailwaveImported has started processing the following file(s):", 'information');
        }
    foreach($import as $filename) {   
        if (defined('SI_admin_access')) {
        $application->enqueueMessage($filename."</ br>", 'information');
        }
        $move = rename ($sailwaveFolder .  "/". $filename, $processingFolder . "/" . $filename );
       
        if (!$move) {
            if (defined('SI_admin_access')) {
            $application->enqueueMessage("<b>".$filename." could not be moved to the processing folder", 'error');
            }
        }
    }
}

//Scan the processing folder a worklist of files to review for import
$import = array_diff(scandir($processingFolder), $ignoredFiles);
//Exclude directories (shouldn't be any but if there are things will break)
$import = array_filter( $import, function ($item) use($sailwaveFolder){
        return !is_dir($sailwaveFolder. "/" . $item );
    });

if (!empty($import)) {
    foreach ($import as $filename) {
        $error = FALSE;
        //Double check file is readable by webserver
        if (!(is_readable($processingFolder."/".$filename))) {
            $error = TRUE;
            if (defined('SI_admin_access')) {
                $application->enqueueMessage($filename." is not readable - processing failed. It is most likely you uploaded this as an FTP user which does not have read rights for the webserver. Consult your administrator / web host for advice.", 'error');
            }
            if ($notifyQuarantine ==1 ) {
                    $emailNotifyBody = $filename." is not readable - processing failed. It is most likely you uploaded this as an FTP user which does not have read rights for the webserver. Consult your administrator / web host for advice.";
                    emailAdmin($emailNotifyBody, $emailQuarantine);
                    //Move file to quarantine
                    rename ($processingFolder . "/" . $filename, $quarantineFolder . "/" . $filename);
                    
                }
        break ;
            }
        $resultFile = new DOMDocument() ;
        $resultFile ->loadHTMLFile($processingFolder . "/" . $filename);
        $last_libxmlerror = libxml_get_last_error();
        //Check all required tags are present
        foreach ($requiredTags as $tagName) {
            $tags = NULL;
            $tags = $resultFile ->getElementsByTagName($tagName);
            if ($tags->length == 0){
                //Error - the tag was required
                if (defined('SI_admin_access')) {
                $application->enqueueMessage($filename." does not contain the required ".$tagName. " - processing failed. File moved to quarantine.", 'error');
                }
                $emailNotifyBody = $filename." does not contain the required ".$tagName. " - processing failed. File moved to quarantine.";
                $emailNotifyBody .="\n\n".$resultFile->saveHTML();
                if ($notifyQuarantine == 1){
                    emailAdmin($processingFolder.$emailNotifyBody, $emailQuarantine);
                }
                //Move file to quarantine
                rename ($processingFolder . "/" . $filename, $quarantineFolder . "/" . $filename);
                

                
                $error = TRUE;
                break; //stop searching the file
            } 
            
        if ( $error == FALSE ) {
            if (defined('SI_admin_access')) {
            $application->enqueueMessage($filename." contain the required tags - processing continues.", 'information');
            }
            // Check file doesn't contain any prohibited content.
            $text = $resultFile->saveXML();
            foreach ($prohibitedContent as $needle)
            if ( strpos ($text, $needle ) != FALSE ) {
                //Error - the tag was required
                if (defined('SI_admin_access')) {
                $application->enqueueMessage($filename." contains the prohibitted phrase ".$needle. " - processing failed. File moved to quarantine.", 'error');
                }
                $emailNotifyBody = $filename." contains the prohibitted phrase ".$needle. " - processing failed. File moved to quarantine.";
                if ($notifyQuarantine == 1){
                    emailAdmin($emailNotifyBody, $emailQuarantine);
                }
                //Move file to quarantine
                rename ($processingFolder . "/" . $filename, $quarantineFolder . "/" . $filename);
                
                
                $error = TRUE;
                
                break;
            }
            
        }
    }
        if ($error == FALSE) {
            
            $tags = $resultFile ->getElementsByTagName('sailwave_id');
            $sailwaveID = $tags->item(0)->nodeValue;
            
            $alias = "results-" . $sailwaveID;
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select($db->quoteName(array('id','catid','alias','title','introtext','fulltext','state')));
            $query->from($db->quoteName('#__content'));
            $query->where($db->quoteName('alias').' = \''.$alias .'\'');
//    
            
            $db->setQuery($query);
            
            $queryResult = $db->loadAssoc();
            
            
            if ($queryResult['alias'] != $alias){
                    $id = 0;
            } else {
                $id = $queryResult['id'];
            }
            //Import the result
            $eventtitle = $resultFile ->getElementsByTagName('sailwave_name');
            $eventtitle = $eventtitle->item(0)->nodeValue;
            $eventtitle_start = substr($eventtitle, 0, 1);
            if ($eventtitle_start =="*"){
                $eventtitle = substr($eventtitle, 1, -1);
            }
            $title = "Results for ".$eventtitle;
           
            $xpath= new DOMXPath ($resultFile);
            #$xpath= simplexml_load_string($resultFile);
            
            $seriestitles = $xpath->query('//h3[@class="seriestitle"]');
            $provisional = $seriestitles->item(0)->nodeValue;
            //remove from the body (if it exists)
            if ($provisional != "") {
                    $seriestitles->item(0)->parentNode->removeChild($seriestitles->item(0));
            }
            if ($id!= 0) {
                $intro = "Updated results for the ".$eventtitle." have been published.  ".$provisional;
            } else {
                $intro = "Results for the ".$eventtitle." have been published.  ".$provisional;
            }
            if ($notesAsIntro) {
                if ($xpath->evaluate('boolean(//p[@class="seriesnotes"])')) {
                    $seriesNotes = $xpath->query('//p[@class="seriesnotes"]');
                    $seriesNote = $seriesNotes ->item(0)->nodeValue;
                    $intro .= "<p>" . $seriesNote . "</p>";
                    //remove from the body
                    $seriesNotes->item(0)->parentNode->removeChild($seriesNotes->item(0));
                }
            }
 
            $body = $resultFile ->getElementById('wrap');
           
            $body = $resultFile ->saveHTML($body); //NB - re-writes to resultFile - pull any data before this
            
            if ($feature == 1) {
                if ($eventtitle_start != "*") {
                    $feature = 0;
                }
            } else if ($feature == 2) {
                $feature =1;
            } 
        
            
            
            //file is good to import :-D
            $app = JFactory::getApplication('site');
            $article_data = array (
                'id' => $id,
                'catid' => $resultsCat,
                'title' => $title,
                'alias' => $alias,
                'introtext' => $intro,
                'fulltext' => $body,
                'state' => 1,
                'language' => "*",
                'access' => 1,
                'featured' => $feature,
                
                
            );
            $data['rules'] = array (
                'core.edit.delete' => array(),
                'core.edit.edit' => array(),
                'core.edit.state' => array()
            );
            
            require_once JPATH_ADMINISTRATOR. '/components/com_content/models/article.php';
            $config=array();
            $article_model = new ContentModelArticle ($config);
            if ($article_model->save($article_data)) 
            {
                if (defined('SI_admin_access')) {
                echo "<h2>Article created with id:".$article_model->getItem()->id."</h2>";
                }
            } else {
                if (defined('SI_admin_access')) {
                echo "ERROR:<br/>".
                        $article_model->getError();
                }
            }
            
        }
    //Copy the imported file to the import folde
    rename ($processingFolder ."/". $filename, $importedFolder . "/". $filename)   ; 
    
    }
}
