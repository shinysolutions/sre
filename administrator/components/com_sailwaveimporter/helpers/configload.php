<?php

/* 
# version: 0.31
# author: Shiny Solutions
# copyright Copyright (C) 2016 Shiny Solutions. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/


defined('_JEXEC') or die("Restricted access");

$application = JFactory::getApplication();
$importer_params = JComponentHelper::getParams('com_sailwaveimporter');

$sailwaveFolder = JPATH_SITE . "/" . $importer_params->get('sailwave_folder', 'SailwaveResults');
$quarantineFolder = $sailwaveFolder . "/" . "Quarantine";
$processingFolder = $sailwaveFolder . "/" . "Processing";
$importedFolder = $sailwaveFolder . "/" . "Imported";
$htaccessFile = $sailwaveFolder . "/" . ".htaccess";
$htaccessContent = "deny from all";
$permitted_ext= json_decode($importer_params->get('permitted_extensions')); 
$permittedExtensions = $permitted_ext->extension;
  
$ignoredFiles = array_map('strtolower' , array ('.', '..', '.htaccess'));
$notifyQuarantine = $importer_params->get('notify_quarantine', '') ;
$emailQuarantine = $importer_params->get('notify_email', '');;
$emailNotifyBody = "Some files in the SailwaveResults Folder at: "."have been uploaded which require manual intervention to import them. \n \n Please login to the site and review the following files: \n";
$required_cont= json_decode($importer_params->get('required_content'));
$requiredTags = $required_cont->required_tag;
$prohibited_cont= json_decode($importer_params->get('prohibited_content', '{"prohibited":["&lt;?"]}'));
$prohibitedContent = $prohibited_cont->prohibited;
$resultsCat = $importer_params->get('results_category', '');
$notesAsIntro = $importer_params->get('notes_in_intro', '');
$feature = $importer_params->get('feature_article', '1') ;
