<?php

/* 
# version: 0.31
# author: Shiny Solutions
# copyright Copyright (C) 2016 Shiny Solutions. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/


defined('_JEXEC') or die("Restricted access");
include (JPATH_BASE.'/components/com_sailwaveimporter/helpers/configload.php');
$xys="XYZ";
//Check folders exist
$sailwaveFolderMSG = "";
if (!is_dir($sailwaveFolder)) {
    //folder does not exist so create it
    mkdir($sailwaveFolder);
        $sailwaveFolderMSG .= "did not exist.  It has therefore been created";
        //echo "<span class='info'>No ". $sailwaveFolder ." one has been created</span><br />";
    
    //and add an htaccess file to it to prevent it being directly accessible
    file_put_contents($htaccessFile, $htaccessContent );
        //echo "<span class='info'>htaccess file created</span><br />";
    
} else {
        $sailwaveFolderMSG .= "exists.";
}
if (!file_exists($htaccessFile)) {
    // report if there is no htaccess folder
    //echo "<span class='warning'>No htaccess file"<span class='warning'> exists in ". $sailwaveFolder ." this could be a security risk. If someone compromises the SailwaveFolder's FTP password (FTP does not use encryption) they could upload a PHP file and execute the script from this folder.  Doing so could allow your site to be compromised.  Add a .htaccess file to the folder with the single line <pre>deny from all</pre>.</span><br />";
    $sailwaveFolderMSG .= "<span class='warning'>No htaccess file exists in ". $sailwaveFolder ." this could be a security risk. If someone compromises the SailwaveFolder's FTP password (FTP does not use encryption) they could upload a PHP file and execute the script from this folder.  Doing so could allow your site to be compromised.  Add a .htaccess file to the folder with the single line <pre>deny from all</pre>.</span>";
        
} else {
    $sailwaveFolderMSG .= " <span class='icon-publish'></span>  An htaccess file is present in this folder to protect against direct access.";
}

$quarantineFolderMSG = "";
if (!is_dir($quarantineFolder)) {
    //folder does not exist so create it
    mkdir($quarantineFolder);
    echo "<span class='info'>No ". $quarantineFolder ." one has been created</span><br />";
    $quarantineFolderMSG .= "No quarantine folder ".$quarantineFolder." was found so one was created.";
} else {
    $quarantineFolderMSG .= "<span class='icon-publish'></span> Quarantine folder ".$quarantineFolder." was found.";
}

$processingFolderMSG = "";
if (!is_dir($processingFolder)) {
    //folder does not exist so create it
    mkdir($processingFolder);
        echo "<span class='info'>No ". $processingFolder ." one has been created</span><br />"; 
        $processingFolderMSG .= "No ".$processingFolder ." was found.  One has been created.";
} else {
    $processingFolderMSG .= "<span class='icon-publish'></span> Processing folder ".$processingFolder. " was found.";
}

$importedFolderMSG = "";
if (!is_dir($importedFolder)) {
    //folder does not exist so create it
    mkdir($importedFolder);
    echo "<span class='info'>No ". $importedFolder ." one has been created</span><br />";
    $importedFolderMSG .= "No imported files folder ".importedFolder . " was found. One has been created.";
} else {
    $importedFolderMSG .= "<span class='icon-publish'></span> Imported folder ".$importedFolder." was found.";
}
