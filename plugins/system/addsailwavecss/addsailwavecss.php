<?php
/* 
# version: 0.30
# author: Shiny Solutions, derived from work by Impression eStudio
# copyright Copyright (C) 2016 Shiny Solutions. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/

defined('_JEXEC') or die("Restricted access");

jimport( 'joomla.plugin.plugin' );

class plgSystemAddSailwaveCSS extends JPlugin
{	
	function plgSystemAddSailwaveCSS( &$subject, $params )
	{
		parent::__construct($subject, $params);

	}
	
	function onBeforeCompileHead()
	{
		$document = JFactory::getDocument();

		if (strcmp(substr(JURI::base(), -15), "/administrator/")!=0)	// Apply styles only to front-end.
		{
			// **********************************************************************
			// Custom CSS 1.
			// **********************************************************************
			if (strcmp($this->params->get('sailwave_css_file_name'),'')!=0)
			{
				
                                $document = JFactory::getDocument();
                                $document->addStyleSheet( JURI::base() . "media/com_sailwaveimporter/CSS/". $this->params->get('sailwave_css_file_name') . ".css");

			}
		
		}
	}
}
