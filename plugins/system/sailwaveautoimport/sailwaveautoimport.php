<?php
/* 
# version: 0.30
# author: Shiny Solutions
# copyright Copyright (C) 2016 Shiny Solutions. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/

defined('_JEXEC') or die("Restricted access");

jimport( 'joomla.plugin.plugin' );

class plgSystemSailwaveAutoImport extends JPlugin
{	
	function plgSystemSailwaveAutoImport( &$subject, $params )
	{
		parent::__construct($subject, $params);

	}
	
	function onAfterInitialise()
	{   
                //get the plugin params
                //$plugin = JPluginHelper::getPlugin('system', 'sailwaveautoimport');
                //$params = new JRegistry($plugin->params);
                
                
                $lastRun = new DateTime($this->params -> get('sailwave_auto_last_ran'));
                $frequency =  $this->params -> get('sailwave_auto_frequency', 10);
                $runDue = $lastRun;
                $runDue->add(new DateInterval("PT".$frequency."M"));
                $now = new DateTime("NOW");
                
                if ($runDue < $now) {
                    // Scan for files is due
                    // Start Scanning 
                    // Start by resetting the last_run time to now  
                    $table = new JTableExtension(JFactory::getDbo());
                    $table->load(array('element' => 'sailwaveautoimport'));
                    $this->params->set('sailwave_auto_last_ran', date_format($now, "Y-m-d H:i:s" )); 
                    $table->set('params', $this->params->toString());
                    $table->store();
                    
                    // run the import script
                    require_once JPATH_ADMINISTRATOR.'/components/com_sailwaveimporter/helpers/importer.php';
                   
                    
                } else {
                    // Scan for files not due
                    
                    return;
                }
                
                return;

	}
}
